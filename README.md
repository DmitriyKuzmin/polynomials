# Polynomials
Calculate expression
result = 1/n*3 + 1/n*3 + 1/n*3 + ...

# Demo
Setting arguments:

![sets arguments](./images/1.png)

If no arguments:

![no arguments](./images/2.png)

Result:

![result](./images/3.png)

# Data 
Input

 * denominator ratios by arguments command line
    * Example input:  1,2,3,4,5
    
 Output
 
 *  expression result